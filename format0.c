/* a simple text formatter */

#include <stdio.h>
#include <stdlib.h>

#define MAXCHAR 300       /* maximum number of characters */
#define MAXWORD  50      /* maximum number of words */

int  linelength;          /* number of characters per line */
char buffer[MAXCHAR+1];   /* buffer to save words */
int  cp, wc;int  wordhead[MAXWORD+1], wordtail[MAXWORD+1]; /* word position */

int  ch;
int blank;
int pagenumber;
int flag;
int n;
int ep;

struct margin_struct
{
  int  length;
  char chars[MAXCHAR+1];
} margin, nextmargin;     /* left margin */

  void initialize(void)
  {
    // １行の文字数
    linelength = 79;
    cp = 0;
    wc = 0;
    wordhead[0] = 0;
    wordtail[0] = 0;
    margin.length = 0;
    nextmargin.length = 0;
    // 先頭の空白の数
    blank = 0;
    // ページの行の番号
    pagenumber = 1;
    // flagが1のとき入力テキストをそのまま標準出力。
    // flagが0のとき整形して出力。
    flag = 0;
    ep = 0;
  }

  //pagefooter:61行目にページ数を出力する関数。
  void pagefooter(void){
    int i = 0;
    // あるページ59行目に達したときに61行目でページ数を出力。
    if((pagenumber % 59) == 0){ 
      // 60行目は空白
      printf("\n");
      // ページ番号を出力。
      for(i = 0;i < linelength / 2; i++){
        printf("-");
      }
      printf("%d",pagenumber/59);
      for(i = 0;i < linelength / 2; i++){
        printf("-");
      }
      printf("\n");
    }
    // 次の行へ
    pagenumber++;
  }



  void fill(void)
  {
    int i, j;
    int blanks, gaps, w;

    for(j = 1; j <= margin.length ;++j)
      putchar(margin.chars[j]);
    if((wc == 1) || (wc == 2)) {
      for(j = 1; j <= wordtail[1];++j)
        putchar(buffer[j]);
      for(j = wordtail[1]+1; j <= wordtail[wc];++j)
        buffer[j-wordtail[1]] = buffer[j];
      cp = wordtail[wc]-wordtail[1];
      wordhead[1] = 1;
      wordtail[1] = cp;
      wc = wc-1;
    }
    else {
      blanks = linelength-margin.length-wordtail[wc-1];
      gaps = wc-2;
      for(i = 1; i <= wc-2; ++i) {
        for(j = wordhead[i]; j <= wordtail[i]; ++j)
          putchar(buffer[j]);
        w = blanks/gaps;
        // 偶数のときのみ空白を一つ増やす。
        if((pagenumber % 2 ) ==  0 && (blanks%gaps) != 0){
         w++;
       }
       // 空白を出力します。
       for(j = 1; j<= w; ++j) printf("%c", ' ');
        blanks = blanks-w;
      gaps = gaps-1;
    }
    for(j = wordhead[wc-1]; j <= wordtail[wc-1]; ++j){
     putchar(buffer[j]);
   }
   for(j = wordhead[wc]; j <= wordtail[wc]; ++j)
     buffer[j-wordtail[wc-1]] = buffer[j];
   cp = wordtail[wc]-wordtail[wc-1];
   wordhead[1] = 1;
   wordtail[1] = cp;
   wc = 1;
 }
 putchar('\n');
 pagefooter();
 for(j = 1; j <= nextmargin.length; ++j)
  margin.chars[j] = nextmargin.chars[j];
margin.length = nextmargin.length;
}

void wordboundary(void)
{
  if(cp > wordtail[wc]) {
    if(wc >= MAXWORD) {
      fprintf(stderr, "\nToo many words to be saved\n");
      exit(1);
    }
    wc = wc+1;
    wordhead[wc] = wordtail[wc-1]+1;
    wordtail[wc] = cp;
    if (cp+(wc-1) > linelength-margin.length)
      fill();
  }
}

void addcharacter(int c)
{
  if(cp >= MAXCHAR) {
    fprintf(stderr, "\nToo many characters to be saved\n");
    exit(1);
  }
  ++cp;
  buffer[cp] = c;
}

// 改行です。
void flush(void)
{
  int i, j;
  if(wc > 0) {
    for(i = 1; i <= margin.length; ++i)
      putchar(margin.chars[i]);
    for(i = 1; i <= wc; ++i) {
      for(j = wordhead[i]; j <= wordtail[i]; ++j)
       putchar(buffer[j]);
     putchar(' ');
   }
   putchar('\n');
   pagefooter();
   cp = 0;
   wc = 0;
   if(ep == 0){
   margin.length = 0;
   nextmargin.length = 0;
   }  
 }
}

void brcommand(void)
{
  // flushは強制的に改行をするのでflush関数を呼び出します。
  flush();
}

void blcommand(void)
{
  // brcommand()で呼び出すとwcなどが0になる。
  brcommand();
  putchar('\n');
  pagefooter();
}

void npcommand(void)
{
  int i = 0;
  // brcommand()を呼び出します。
  brcommand();
  // blankは空行の数なのでそれまで空白をmargin.chars[i]に格納いたします。
  // margin.lengthも空行の数です。
  // 後にfill関数で以下のように出力。
  // for(j = 1; j <= margin.length ;++j)
  // putchar(margin.chars[j]);
  for(i = 0;i < blank; i++){
    margin.length++;
    margin.chars[i] = ' ';
  }
}

// 先頭の空白の数を設定する関数です。
void incommand(n){
  blank = n;
}

// １行の文字数を設定します。
void llcommand(n){
  linelength = n;
}

// nfコマンドが呼ばれた時はflagを1にして入力テキストをそのまま出力します。
void nfcommand(){
  flag = 1;
}

// fiコマンドが呼ばれたときはもとに戻します。
void ficommand(){
  flag = 0;
}


void ipcommand(void)
{
  int i;
  // 初期化
  brcommand();

  //最初の空白を挿入
  margin.chars[margin.length] = ' ';
  margin.length++;

  //段落番号を挿入
  ch = getchar();
  while(ch != '\n' && ch != EOF) {
    margin.chars[margin.length] = ch;
    margin.length++;
    ch = getchar();
  }

  // 空白を２つ開ける
  margin.chars[margin.length] = ' ';
  margin.length++;

  margin.chars[margin.length] = ' ';
  margin.length++;

  // 次の行の先頭の空白はmargin.length分だけ空白があります。
  for(i = 0; i < margin.length; i++){
    nextmargin.chars[i] = ' ';
    nextmargin.length++;
  }
  ep = 1;
}

//ipコマンドの整形モードを解除するコマンドです。
void epcommand(void){  
  ep = 0;
}



void eccommand(void){
  ch = getchar();
  // その後の単語をすべて１つの単語として見る。
  while(ch != '\n' && ch != EOF) {
    addcharacter(ch);
    ch = getchar();
  }
  // １つの単語の終了。
  wordboundary();
  // 改行をして初期化します。
  flush();
}



void cecommand(void){
  int i;
  // 初期化
  brcommand();

  // 単語をバッファーに入れます。
  ch = getchar();
  while(ch != '\n' && ch != EOF) {
    if(ch == ' '){
      wordboundary();
    }else{
      addcharacter(ch);
    }
    ch = getchar();
  }
  wordboundary();

  // １つの単語の文字列が長かった場合の処理
  if(wordtail[1] - wordhead[1] > linelength){
    fprintf(stderr, "\nToo many characters to be saved　against one line\n");
    exit(1);
  }

  for(i = 0; i < (linelength - cp) / 2; i++){
    margin.chars[i] = ' ';
    margin.length++;
  }
  brcommand();
}

// 段落の２行目
void ifcommand(void)
{
  int i;
  brcommand();
  // 先頭は空白
  margin.chars[margin.length] = ' ';
  margin.length++;
  // 文字列をmarginに入れます。
  ch = getchar();
  while(ch != '\n' && ch != EOF) {
    margin.chars[margin.length] = ch;
    margin.length++;
    ch = getchar();
  }

  // 次に２つの空白を入れます。
  margin.chars[margin.length] = ' ';
  margin.length++;

  margin.chars[margin.length] = ' ';
  margin.length++;

  // ２行目以降の先頭の空白は8個までに設定します。
  for(i = 0; i < 8; i++){
    nextmargin.chars[i] = ' ';
    nextmargin.length++;
  }
}

void hrcommand(n){
  putchar(n);
}

void command(void)
{
  int  first, second;
  int n;

  if ((ch = getchar()) != '\n') {
    first = ch;
    if ((ch = getchar()) != '\n') {
      second = ch;
    // 改行を出力するコマンドであるbrコマンドを作成いたします。
      if((first == 'b') && (second == 'r'))
       brcommand();
     //先頭の空白数を調整するこまんどnpを作成します。
     else if ((first == 'n') && (second == 'p'))
       npcommand();
     //空白の行を作成するコマンドであるblコマンドを追加いたします。
     else if ((first == 'b') && (second == 'l'))
       blcommand();
     // 1行を中央に配置するコマンドであるceコマンドを追加いたします。
     else if ((first == 'c') && (second == 'e'))
       cecommand();
    //箇条書きの段落を作成するコマンドであるipコマンドを追加いたします。
     else if ((first == 'i') && (second == 'p'))
       ipcommand();
    // テキストと同じ出力をするコマンドnfコマンドを追加いたします。
     else if ((first == 'n') && (second == 'f'))
       nfcommand();
    //コマンドを出力するコマンドであるecコマンドを追加いたします。
     else if ((first == 'e') && (second == 'c'))
       eccommand();
      // nfコマンドからフォーマットされた出力に戻すfiコマンドを追加いたします。
     else if ((first == 'f') && (second == 'i'))
       ficommand();
     // ipコマンドを解除するコマンドです。
     else if ((first == 'e') && (second == 'p'))
       epcommand();
     // ２行目以降を指定の段落にするコマンドであるifコマンドを追加いたします。
     else if ((first == 'i') && (second == 'f'))
      ifcommand();
      // 先頭の空白の数を指定するコマンドinコマンドを追加いたします。
    else if ((first == 'i') && (second == 'n')){
        // 空白数を指定。
     scanf("%d",&n);
  // in+スペース+空行数の仕様。
     if((getchar() == ' ')){
       incommand(n);
     }
   }
      // １行に出力する文字数を指定するllコマンドを指定いたします。
   else if ((first == 'l') && (second == 'l')){
        // 文字数を指定。
     scanf("%d",&n);
   // ll+スペース+空行数の仕様。
     if((getchar() == ' ')){
       llcommand(n);
     }
   }
   else if ((first == 'h') && (second == 'r')){
    int i = 0;
    if((getchar() == ' ')){
      ch = getchar();
      while(ch != '\n' && ch != EOF) {
        hrcommand(ch);  
        ch =getchar();
      }
      putchar('\n');
      pagenumber++;
      for(i = 0; i< linelength;i++){
        printf("-");
      }
      printf("\n");
      pagenumber++;
    }  
  }
}
}
}


int main(void)
{
  // 初期化
  initialize();
  // 文書を一文字ずつ処理をしていきます。
  ch = getchar();
  while(ch != EOF) {
    // 一行ずつ処理をします。
    while(ch != '\n' && ch != EOF) {
      // アクサンシルコンフレックス、サーカムフレックスアクセントで始まるときの処理。
      if(ch == '^') {
        // 単語の境目を設定。
        wordboundary();
        // コマンドを取得します。
        command();
      }
      // flagが１のとき（すなわちnfが呼ばれた時)にテキストを文書をそのまま
      // 出力いたします。
      else if(flag == 1){
       printf("%c",ch);
     }
     // スペースが入ったときは単語の境目なのでwordboundary()を出力します。
     else if(ch == ' ')
      wordboundary();
      // 単語をバッファにいれます。
    else
      addcharacter(ch);
    // さらに文字を取得。
    ch = getchar();
  }
  // flagが１のとき（すなわちnfが呼ばれた時)にテキストを文書をそのまま
  // 出力いたします。
  if(flag == 1){
   printf("%c",ch);
 }else{
  wordboundary();
}
ch = getchar();
}
  // 強制的に改行をして終了します。
flush();
return (0);
}
