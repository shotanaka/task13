format.c 田中　翔
-------------------------------------------------------------------------------
                                 2.4 Constants 

There are several kinds of constants, as listed below. Hardware characteristics
which affect sizes are summarized in 2.6. 

 2.4.1   Integer  constants:  An  integer  constant consisting of a sequence of
         digits is taken to be octal if it begins with 0 (digit zero),  decimal
         otherwise. The digits 8 and 9 have octal value 10 and 11 respectively.
         A sequence of digits preceded by 0x or 0X (digit zero) is taken to  be
         a hexadecimal 
         integer. The hexadecimal digits include a or A through  f  or  F  with
         values  10  through  15.  A  decimal  constant whose value exceeds the
         largest signed machine integer is taken to be long; an  octal  or  hex
         constant   which   exceeds   the   largest  unsigned  machine  integer
          is likewise taken to be long. 
          2.4.1   Integer  constants:  An  integer  constant  consisting  of  a
                 sequence of digits is taken to be octal if it  begins  with  0
                 (digit zero), decimal otherwise. The digits 8 and 9 have octal
                 value 10 and 11 respectively. A sequence of digits preceded by
                 0x or 0X (digit zero) is taken to be a hexadecimal 
                 integer. The hexadecimal digits include a or A through f or  F
                 with  values  10  through  15.  A decimal constant whose value
                 exceeds the largest signed machine  integer  is  taken  to  be
                 long;  an  octal  or  hex  constant  which exceeds the largest
                 unsigned machine integer  is likewise taken to be long. 
