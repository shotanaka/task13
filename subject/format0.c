/* a simple text formatter */

#include <stdio.h>
#include <stdlib.h>

#define MAXCHAR 300       /* maximum number of characters */
#define MAXWORD  50       /* maximum number of words */

int  linelength;          /* number of characters per line */
char buffer[MAXCHAR+1];   /* buffer to save words */
int  wordhead[MAXWORD+1], wordtail[MAXWORD+1]; /* word position */
    int  cp, wc;
int  ch;
int blank;
int odd;
int flag;
int n;

struct margin_struct
{
  int  length;
  char chars[MAXCHAR+1];
} margin, nextmargin;     /* left margin */

  void initialize(void)
  {
    // １行の文字数
    linelength = 50;
    cp = 0;
    wc = 0;
    wordhead[0] = 0;
    wordtail[0] = 0;
    margin.length = 0;
    nextmargin.length = 0;
    // 先頭の空白の数
    blank = 6;
    // 行が奇数の場合odd=1
    // 行が偶数の場合odd=0
    odd = 1;
    flag = 0;
  }


void page(void){
  int i;
  if(odd % 59 == 0){
    printf("\n");
    for(i=0;i<linelength/2;i++){
      printf(" ");
    }
    printf("%d\n\n",odd/59);
  }
  odd++;
}




  void fill(void)
  {
    int i, j;
    int blanks, gaps, w;

    for(j = 1; j <= margin.length ;++j)
      putchar(margin.chars[j]);
    if((wc == 1) || (wc == 2)) {
      for(j = 1; j <= wordtail[1];++j)
        putchar(buffer[j]);
      for(j = wordtail[1]+1; j <= wordtail[wc];++j)
        buffer[j-wordtail[1]] = buffer[j];
      cp = wordtail[wc]-wordtail[1];
      wordhead[1] = 1;
      wordtail[1] = cp;
      wc = wc-1;
    }
    else {
      blanks = linelength-margin.length-wordtail[wc-1];
      gaps = wc-2;
      for(i = 1; i <= wc-2; ++i) {
        for(j = wordhead[i]; j <= wordtail[i]; ++j)
          putchar(buffer[j]);
        w = blanks/gaps;
        // 偶数のときのみ空白を一つ増やす。
        if(odd % 2 == 0 && (blanks%gaps) != 0){
         w++;
       }
       // 空白を出力します。
       for(j = 1; j<= w; ++j) printf("%c", ' ');
        blanks = blanks-w;
      gaps = gaps-1;
    }
    for(j = wordhead[wc-1]; j <= wordtail[wc-1]; ++j){
     putchar(buffer[j]);
   }
   for(j = wordhead[wc]; j <= wordtail[wc]; ++j)
     buffer[j-wordtail[wc-1]] = buffer[j];
   cp = wordtail[wc]-wordtail[wc-1];
   wordhead[1] = 1;
   wordtail[1] = cp;
   wc = 1;
 }
 putchar('\n');
 page();
 for(j = 1; j <= nextmargin.length; ++j)
  margin.chars[j] = nextmargin.chars[j];
margin.length = nextmargin.length;
}

void wordboundary(void)
{
  if(cp > wordtail[wc]) {
    if(wc >= MAXWORD) {
      fprintf(stderr, "\nToo many words to be saved\n");
      exit(1);
    }
    wc = wc+1;
    wordhead[wc] = wordtail[wc-1]+1;
    wordtail[wc] = cp;
    if (cp+(wc-1) > linelength-margin.length)
      fill();
  }
}

void addcharacter(int c)
{
  if(cp >= MAXCHAR) {
    fprintf(stderr, "\nToo many characters to be saved\n");
    exit(1);
  }
  ++cp;
  buffer[cp] = c;
}

// 改行です。
void flush(void)
{
  int i, j;
  if(wc > 0) {
    for(i = 1; i <= margin.length; ++i)
      putchar(margin.chars[i]);
    for(i = 1; i <= wc; ++i) {
      for(j = wordhead[i]; j <= wordtail[i]; ++j)
       putchar(buffer[j]);
     putchar(' ');
   }
   putchar('\n');
   cp = 0;
   wc = 0;
   margin.length = 0;
   nextmargin.length = 0;
   page();
 }
}

void brcommand(void)
{
  // flushは強制的に改行をするのでflush関数を呼び出します。
  flush();
}

void blcommand(void)
{
  // brcommand()で呼び出すとwcなどが0になる。
  brcommand();
  putchar('\n');
  page();
}

void npcommand(void)
{
  int i = 0;
  // brcommand()を呼び出します。
  brcommand();
  // blankは空行の数なのでそれまで空白をmargin.chars[i]に格納いたします。
  // margin.lengthも空行の数です。
  // 後にfill関数で以下のように出力。
  // for(j = 1; j <= margin.length ;++j)
  // putchar(margin.chars[j]);
  for(i = 0;i < blank; i++){
    margin.length++;
    margin.chars[i] = ' ';
  }
}

// 先頭の空白の数を設定する関数です。
void incommand(n){
  blank = n;
}

// １行の文字数を設定します。
void llcommand(n){
  linelength = n;
}

// nfコマンドが呼ばれた時はflagを1にして入力テキストをそのまま出力します。
void nfcommand(){
  flag = 1;
}

// fiコマンドが呼ばれたときはもとに戻します。
void ficommand(){
  flag = 0;
}

void cecommand(void)
{
  int i;
  brcommand();
  ch = getchar();
  while(ch != '\n' && ch != EOF){
    if(ch == ' '){
      wordboundary();
    }else{
      addcharacter(ch);
    }
    ch = getchar();
  }
  wordboundary();
  if(wordtail[1] - wordhead[1] > linelength){
    fprintf(stderr, "your word is over wordlength\n");
  }
  for(i = 1;i < (linelength - cp ) / 2; i++){
    margin.chars[i] = ' ';
    margin.length++;
  }
  flush();
}

void ipcommand(void)
{ 
  int i;
  margin.length++;
  margin.chars[i] = ' ';
  ch = getchar();
  while(ch != '\n' && ch != EOF){
    margin.length++;
    margin.chars[margin.length] = ch;
    ch = getchar();
  }
  for(i=0;i<2;i++){
    margin.length++;
    margin.chars[margin.length] = ' ';
  }
  for(i=1;i<margin.length;i++){
    nextmargin.length++;
    nextmargin.chars[i] = ' ';
  }
}
void ifcommand(void)
{ 
  int i;
  margin.length++;
  margin.chars[i] = ' ';
  ch = getchar();
  while(ch != '\n' && ch != EOF){
    margin.length++;
    margin.chars[margin.length] = ch;
    ch = getchar();
  }
  for(i=0;i<2;i++){
    margin.length++;
    margin.chars[margin.length] = ' ';
  }
  for(i=1;i<8;i++){
    nextmargin.length++;
    nextmargin.chars[i] = ' ';
  }
}

void ecommand(void){
  ch = getchar();
   while(ch != '\n' && ch != EOF){
    addcharacter(ch);
    ch = getchar();
  }
  wordboundary();
}

void command(void)
{
  int  first, second;
  int n;

  if ((ch = getchar()) != '\n') {
    first = ch;
    if ((ch = getchar()) != '\n') {
      second = ch;
      if((first == 'b') && (second == 'r'))
       brcommand();
     else if ((first == 'n') && (second == 'p'))
       npcommand();
     else if ((first == 'b') && (second == 'l'))
       blcommand();
     else if ((first == 'c') && (second == 'e'))
       cecommand();
     else if ((first == 'e') && (second == 'c'))
       ecommand();
     else if ((first == 'i') && (second == 'p'))
       ipcommand();
      // テキストと同じ出力をするコマンドnfコマンドを追加いたします。
     else if ((first == 'n') && (second == 'f'))
       nfcommand();
      // nfコマンドからフォーマットされた出力に戻すfiコマンドを追加いたします。
     else if ((first == 'f') && (second == 'i'))
       ficommand();
     else if ((first == 'i') && (second == 'f'))
       ifcommand();
      // 先頭の空白の数を指定するコマンドinコマンドを追加いたします。
     else if ((first == 'i') && (second == 'n')){
        // 空白数を指定。
       scanf("%d",&n);
  // in+スペース+空行数の仕様。
       if((getchar() == ' ')){
         incommand(n);
       }
     }
      // １行に出力する文字数を指定するllコマンドを指定いたします。
     else if ((first == 'l') && (second == 'l')){
        // 文字数を指定。
       scanf("%d",&n);
   // ll+スペース+空行数の仕様。
       if((getchar() == ' ')){
         llcommand(n);
       }
     }
   }
 }
}




int main(void)
{
  int i;
  // 初期化
  initialize();
  // 文書を一文字ずつ処理をしていきます。
  ch = getchar();
  while(ch != EOF) {
    // 一行ずつ処理をします。
    while(ch != '\n' && ch != EOF) {
      // アクサンシルコンフレックス、サーカムフレックスアクセントで始まるときの処理。
      if(ch == '^') {
        // 単語の境目を設定。
        wordboundary();
        // コマンドを取得します。
        command();
      }
      // flagが１のとき（すなわちnfが呼ばれた時)にテキストを文書をそのまま
      // 出力いたします。
      else if(flag == 1){
       printf("%c",ch);
     }
     // スペースが入ったときは単語の境目なのでwordboundary()を出力します。
     else if(ch == ' ')
      wordboundary();
      // 単語をバッファにいれます。
    else
      addcharacter(ch);
    // さらに文字を取得。
    ch = getchar();
  }
  // flagが１のとき（すなわちnfが呼ばれた時)にテキストを文書をそのまま
  // 出力いたします。
  if(flag == 1){
   printf("%c",ch);
  }else{
  wordboundary();
  }
  ch = getchar();
  }
  // 強制的に改行をして終了します。
flush();
return (0);
}
